package akakom.id.akakomweek3;

import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface DaerahService {

    @GET("provinsi")
    Call<ResponProvinsi> listProvinsi();

    @GET("provinsi/{id}/kabupaten")
    Call<ResponKabupaten> listKabupaten(@Path("id") String id);

    @GET("provinsi/kabutpaten/{id}/kecamatan")
    Call<List<Kecamatan>> listKecamatan(@Path("id") String id);

    @GET("province")
    Call<RajaOngkirProvince> listProvince(@Header("key") String apikey);

    @POST("cost")
    Call<ResponseCost> kalkulasiCost(@Header("key") String apiKey,
                               @Body Query requestBody);

    @FormUrlEncoded
    @POST("cost")
    Call<ResponseCost> kalkulasiCost(@Header("key") String apiKey,
                               @Field("origin") int asal,
                               @Field("destination") int tujuan,
                               @Field("weight") int berat,
                               @Field("courier") String nama);



}


















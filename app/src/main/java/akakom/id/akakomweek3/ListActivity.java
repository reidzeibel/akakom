package akakom.id.akakomweek3;

import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        final List<RajaOngkirProvince.Province> list = new ArrayList<>();

        final MenuAdapter adapter = new MenuAdapter(list);
        RecyclerView view = findViewById(R.id.recyclerview);
        view.setLayoutManager(new LinearLayoutManager(this));
        view.setAdapter(adapter);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.rajaongkir.com/starter/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        DaerahService service =
                retrofit.create(DaerahService.class);

        Query query = new Query(501, 114, 2000, "jne");

        service.kalkulasiCost("fa48d17407b728d29dd2e292fb37f222",
                query)
                .enqueue(new Callback<ResponseCost>() {
                    @Override
                    public void onResponse(Call<ResponseCost> call, Response<ResponseCost> response) {
                        Log.e("####", "Success");
                        ResponseCost cost = response.body();
                        String service = cost.rajaongkir.results.get(0).costs.get(0).service;
                        Log.e("####", service);
                    }

                    @Override
                    public void onFailure(Call<ResponseCost> call, Throwable t) {
                        Log.e("####", "Failed");
                    }
                });














        service.listProvince("fa48d17407b728d29dd2e292fb37f222")
                .enqueue(new Callback<RajaOngkirProvince>() {
            @Override
            public void onResponse(
                    Call<RajaOngkirProvince> call,
                    Response<RajaOngkirProvince> response) {

                RajaOngkirProvince hasil = response.body();

                if (hasil.rajaongkir.status.code.equals("200")) {
                    // SUKSES
                    list.addAll(hasil.rajaongkir.results);
                    adapter.notifyDataSetChanged();
                } else {
                    // GAGAL
                }
            }

            @Override
            public void onFailure(
                    Call<RajaOngkirProvince> call,
                    Throwable t) {

            }
        });

        /**
         * Synchronous call, tidak boleh dipanggil di Main Thread.
         *
         try {
         Response<ResponProvinsi> respon = service.listProvinsi().execute();
         Log.e("####", respon.message());
         } catch (IOException e) {
         e.printStackTrace();
         }
         */
//        service.listProvinsi()
//                .enqueue(new Callback<ResponProvinsi>() {
//                    @Override
//                    public void onResponse
//                            (Call<ResponProvinsi> call,
//                             Response<ResponProvinsi> response) {
//
//                        //ambil objek JSON "ResponProvinsi"
//                        ResponProvinsi respon = response.body();
//
//                        if (!respon.error) {
//                            for (Provinsi provinsi : respon.semuaprovinsi) {
//                                Log.d("####", provinsi.nama);
//                            }
//                            list.addAll(respon.semuaprovinsi);
//                            adapter.notifyDataSetChanged();
//                        } else if (respon.error) {
//                            // Handle Error
//                        }
//
//                    }
//
//                    @Override
//                    public void onFailure
//                            (Call<ResponProvinsi> call,
//                             Throwable t) {
//
//                    }
//                });

//        list.add(new MenuTest("Ayam Goreng", 2, "Rp 20.000"));
//        list.add(new MenuTest("Bakso Kuah", 1, "Rp 30.000"));
//        list.add(new MenuTest("Sop Iga", 1, "Rp 15.000"));
//        list.add(new MenuTest("Es Teh", 2, "Rp 10.000"));
//        list.add(new MenuTest("Ayam Goreng", 2, "Rp 20.000"));
//        list.add(new MenuTest("Bakso Kuah", 1, "Rp 30.000"));
//        list.add(new MenuTest("Sop Iga", 1, "Rp 15.000"));
//        list.add(new MenuTest("Es Teh", 2, "Rp 10.000"));
//        list.add(new MenuTest("Ayam Goreng", 2, "Rp 20.000"));
//        list.add(new MenuTest("Bakso Kuah", 1, "Rp 30.000"));
//        list.add(new MenuTest("Sop Iga", 1, "Rp 15.000"));
//        list.add(new MenuTest("Es Teh", 2, "Rp 10.000"));
//        list.add(new MenuTest("Ayam Goreng", 2, "Rp 20.000"));
//        list.add(new MenuTest("Bakso Kuah", 1, "Rp 30.000"));
//        list.add(new MenuTest("Sop Iga", 1, "Rp 15.000"));
//        list.add(new MenuTest("Es Teh", 2, "Rp 10.000"));
//        list.add(new MenuTest("Ayam Goreng", 2, "Rp 20.000"));
//        list.add(new MenuTest("Bakso Kuah", 1, "Rp 30.000"));
//        list.add(new MenuTest("Sop Iga", 1, "Rp 15.000"));
//        list.add(new MenuTest("Es Teh", 2, "Rp 10.000"));

//
//        Gson gson = new Gson();
//        Log.d("#### JSON", gson.toJson(list));


////======================================
//        String json = "";
//        try {
//            Resources res = getResources();
//            InputStream in = res.openRawResource(R.raw.json);
//
//            byte[] b = new byte[in.available()];
//            in.read(b);
//            json = new String(b);
//        } catch (Exception e) {
//            // e.printStackTrace();
//        }
////=======================================
//        Gson gson = new Gson();
//        list = gson.fromJson(json,
//                TypeToken.getParameterized(ArrayList.class, MenuTest.class).getType());

    }
}









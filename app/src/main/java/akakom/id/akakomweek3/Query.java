package akakom.id.akakomweek3;

public class Query {
    int origin;
    int destination;
    int weight;
    String courier;

    public Query(int origin, int destination, int weight, String courier) {
        this.origin = origin;
        this.destination = destination;
        this.weight = weight;
        this.courier = courier;
    }
}


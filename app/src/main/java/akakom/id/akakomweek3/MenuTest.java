package akakom.id.akakomweek3;

public class MenuTest {

    String namaMenu;  // Nama Menu -> Text
    int jumlahPesanan; // Jumlah Pesanan -> Bilangan Bulat
    String hargaMenu; // Harga -> Angka desimal
    String imageUrl; // URL/link gambar menu

    public MenuTest(String namaMenu, int jumlahPesanan, String hargaMenu) {
        this.namaMenu = namaMenu;
        this.jumlahPesanan = jumlahPesanan;
        this.hargaMenu = hargaMenu;
    }
}

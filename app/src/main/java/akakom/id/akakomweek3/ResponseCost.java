package akakom.id.akakomweek3;

import java.util.List;

public class ResponseCost {

    public RajaOngkirCostResult rajaongkir;

    class RajaOngkirCostResult {
        public List<CourierResult> results;
    }

    class CourierResult {
        String code;
        String name;
        public List<CourierService> costs;
    }

    class CourierService {
        public String service;
        public String description;
        public List<ServiceCost> cost;
    }

    class ServiceCost {
        long value;
        String etd;
        String note;
    }


}














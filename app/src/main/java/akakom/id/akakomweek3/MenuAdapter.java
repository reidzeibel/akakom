package akakom.id.akakomweek3;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.MenuHolder> {

    List<RajaOngkirProvince.Province> menuTestList;

    public MenuAdapter (List<RajaOngkirProvince.Province> listItem) {
        menuTestList = listItem;
    }

    @NonNull
    @Override
    public MenuHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_view, parent, false);
        MenuHolder menuHolder = new MenuHolder(v);
        return menuHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MenuHolder holder, int position) {
        RajaOngkirProvince.Province item = menuTestList.get(position);

//        holder.gambar = item.imageUrl;
        holder.namaMenu.setText(item.province);
//        holder.jumlahMenu.setText(Integer.toString(item.jumlahPesanan));
//        holder.hargaMenu.setText(item.hargaMenu);

    }

    @Override
    public int getItemCount() {
        return menuTestList.size();
    }

    static class MenuHolder extends RecyclerView.ViewHolder {

        ImageView gambar;
        TextView namaMenu;
        TextView jumlahMenu;
        TextView hargaMenu;

        public MenuHolder(View itemView) {
            super(itemView);
            gambar = itemView.findViewById(R.id.gambarMenu);
            namaMenu = itemView.findViewById(R.id.namaMenu);
            jumlahMenu = itemView.findViewById(R.id.jumlahMenu);
            hargaMenu = itemView.findViewById(R.id.hargaMenu);

        }
    }
}























